//
//  MyAccountViewController.swift
//  Air
//
//  Created by Vishva Patel on 3/5/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit

class MyAccountViewController: UIViewController {
    
    @IBOutlet var txtName : UITextField!
    @IBOutlet var txtCardNumber : UITextField!
    @IBOutlet var txtExpire : UITextField!
    @IBOutlet var txtCVV : UITextField!
    
    
    let mainDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }
    
    @IBAction func insertCardInfo(sender : Any)
    {
        
        let name = txtName.text
        let Card = txtCardNumber.text
        let Expire = txtExpire.text
        let cvv = txtCVV.text
               
               // Check if required fields are not empty
       if (name?.isEmpty)! || (Card?.isEmpty)! || (Expire?.isEmpty)! || (cvv?.isEmpty)!
               {
                   // Display alert message here
                   let alertController = UIAlertController(title: "Hey?!", message:
                                  "Please Fill Every Field", preferredStyle: .alert)
                       alertController.addAction(UIAlertAction(title: "TRY AGAIN !", style: .default))
                       
                       self.present(alertController, animated: true, completion: nil)
                   
                   return
               }
        
       else{
        
        
        
        let alertController = UIAlertController(title: "Thank You!", message: "Card Data Inserted", preferredStyle: .alert)
               
               let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
               
               alertController.addAction(cancelAction)
               present(alertController, animated: true)
        }
        
    }

}
