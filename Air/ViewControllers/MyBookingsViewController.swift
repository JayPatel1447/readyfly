//
//  MyBookingsViewController.swift
//  Air
//
//  Created by Vishva Patel on 3/5/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit

class MyBookingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let mainDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainDelegate.bookings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell()
        tableCell.textLabel?.text = mainDelegate.bookings[indexPath.row].countryfrom
        tableCell.accessoryType = .disclosureIndicator
        
        return tableCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowNum = indexPath.row
        
        let message = "From: " + mainDelegate.bookings[rowNum].countryfrom! + "\nTo:" + mainDelegate.bookings[rowNum].countryto! + "\nDeparting Date: " + mainDelegate.bookings[rowNum].departingdate! + "\nReturning Date: " + mainDelegate.bookings[rowNum].returningdate! + "\n Flying Class" + mainDelegate.bookings[rowNum].flyclass! + "\nNo of travellers: " + mainDelegate.bookings[rowNum].travellers!
        
        let alertController = UIAlertController(title: "Flight Details", message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
    
    
}
