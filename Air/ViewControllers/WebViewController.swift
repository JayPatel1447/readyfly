//
//  WebViewController.swift
//  Air
//
//  Created by Vishva Patel on 4/10/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet var wbPage : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let urlAddress = URL(string: "https://www.skyscanner.ca")
        let url = URLRequest(url: urlAddress!)
        wbPage?.load(url)
        
    }
    
    
  
}
