//
//  FirstViewController.swift
//  Air
//
//  Created by Vishva Patel on 3/5/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit
import GoogleSignIn


class SignInViewController: UIViewController {
    
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var txtEmail : UITextField!
    
    var mainDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        // Automatically sign in the user.
        //GIDSignIn.sharedInstance()?.restorePreviousSignIn()
       
    }
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        print("Sign in button tapped")
    
        // Read values from text fields
        let email1 = txtEmail.text
        let password1 = txtPassword.text
        
        // Check if required fields are not empty
        if (email1?.isEmpty)! || (password1?.isEmpty)!
        {
            // Display alert message here
            let alertController = UIAlertController(title: "Hey?!", message:
                           "Email & Password Cant be Blank! Try again", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "TRY AGAIN !", style: .default))
                alertController.addAction(UIAlertAction(title: "Register", style: .default){(al) in
                    self.performSegue(withIdentifier: "Register", sender: self)
                })
                self.present(alertController, animated: true, completion: nil)
            
            return
        }
        
        else {
            
        
        let user:NewUsers = mainDelegate.findUser(email: txtEmail.text!, password: txtPassword.text!)
        let email = user.Email
        let password = user.Password
        
        if(email1 == email && password1 == password){
            self.performSegue(withIdentifier: "Login", sender: self)
        }
        else{
                let alertController = UIAlertController(title: "Hello Unknown! ", message:
                           "Wrong Email or Password! Try again", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "TRY AGAIN !", style: .default))
                alertController.addAction(UIAlertAction(title: "Register", style: .default){(al) in
                    self.performSegue(withIdentifier: "Register", sender: self)
                })
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func unwindToSignInVC(segue: UIStoryboardSegue)
    {
        
    }
    
    

}

