//
//  SecondViewController.swift
//  Air
//
//  Created by Vishva Patel on 3/5/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit
import SQLite3




class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    
    var db: OpaquePointer?

    @IBOutlet var firstname : UITextField!
    @IBOutlet var lastname : UITextField!
    @IBOutlet var email : UITextField!
    @IBOutlet var password : UITextField!
    @IBOutlet var repassword : UITextField!
    @IBOutlet var phonenumber : UITextField!
    
    @IBAction func submit(_sender : Any)
    {
        
        let person1 : NewUsers = NewUsers.init()
        person1.initWithData(theRow: 0, theFirstName: firstname.text!, theLastName: lastname.text!, theEmail: email.text!, thePassword: password.text!, theRePassword: repassword.text!, theContact: phonenumber.text!)
        
        let mainDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let returnCode = mainDelegate.insertIntoDatabase(data: person1)
        
        var returnMsg : String  = "Person Added"
        
        if returnCode ==  false{
            returnMsg = "Person add Failed"
        }
        
        let alertController = UIAlertController(title: "SQLITE add", message: returnMsg, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
        
//        let firstname1 = firstname.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//        let lastname1 = lastname.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//        let email1 = email.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//        let password1 = password.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//        let repassword1 = repassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//        let phonenumber1 = phonenumber.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//
//        if(firstname1?.isEmpty)!
//        {
//            print("Name can not be Empty!")
//            return;
//        }
//
//        if(lastname1?.isEmpty)!
//        {
//            print("LastName can not be Empty!")
//            return;
//        }
//
//        if(email1?.isEmpty)!
//        {
//            print("EmailId can not be Empty!")
//            return;
//        }
//
//        if(password1?.isEmpty)!
//        {
//            print("Password can not be Empty!")
//            return;
//        }
//
//        if(repassword1?.isEmpty)!
//        {
//            print("Re Password can not be Empty!")
//            return;
//        }
//
//        if(phonenumber1?.isEmpty)!
//        {
//            print("PhoneNumber can not be Empty!")
//            return;
//        }
//
//        var stnt: OpaquePointer?
//
//        let insertQuery = "INSERT INTO ReadyFly1 (firstname1,lastname1,email1,password1,repassword1,phonenumber1) VALUES (?,?,?,?,?,?)"
//
//        if sqlite3_prepare_v2(db, insertQuery, -1, &stnt, nil) == SQLITE_OK {
//            print("Everything is OKAY till now")
//
//        }
//
//        if sqlite3_bind_text(stnt, 1, firstname1, -1, nil) != SQLITE_OK{
//            print("Error binding name")
//        }
//
//        if sqlite3_bind_text(stnt, 2, lastname1, -1, nil) != SQLITE_OK{
//            print("Error binding name")
//        }
//        if sqlite3_bind_text(stnt, 3, email1, -1, nil) != SQLITE_OK{
//            print("Error binding name")
//        }
//        if sqlite3_bind_text(stnt, 4, password1, -1, nil) != SQLITE_OK{
//            print("Error binding name")
//        }
//        if sqlite3_bind_text(stnt, 5, repassword1, -1, nil) != SQLITE_OK{
//            print("Error binding name")
//        }
//
//        if sqlite3_bind_text(stnt, 6, phonenumber1, -1, nil) != SQLITE_OK{
//            print("Error binding name")
//        }
//
//        if sqlite3_step(stnt) == SQLITE_DONE{
//            print(firstname1 as Any)
//            print("ReadyFly saved successfully")
//        }
//
//        sqlite3_finalize(stnt)
//
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       
    }
        
    
    
    
}



