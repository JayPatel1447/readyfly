//
//  Booking.swift
//  Air
//
//  Created by Vishva Patel on 4/12/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit

class Booking: NSObject {
    var id : Int?
    var countryfrom : String?
    var countryto : String?
    var departingdate : String?
    var returningdate : String?
    var flyclass : String?
    var travellers : String?
    
    func initWithData(theRow i: Int, thecountryfrom cf : String, thecountryto ct : String, thedepartingdate dd: String, thereturningdate rd: String, theflyclass fc: String, thetravellers t: String)
       {
        id = i
        countryfrom = cf
        countryto = ct
        departingdate = dd
        returningdate = rd
        flyclass = fc
        travellers = t
    }
}
