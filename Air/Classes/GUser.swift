//
//  GUser.swift
//  Air
//
//  Created by Vishva Patel on 4/10/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit

class GUser: NSObject {
    var id : Int?
    var FirstName : String?
    var LastName : String?
    var Email : String?
    
    func initWithData(theRow i: Int, theFirstName f : String, theLastName l : String, theEmail e: String)
       {
        id = i
        FirstName = f
        LastName = l
        Email = e
    }
}
