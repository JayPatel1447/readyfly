//
//  Flights.swift
//  Air
//
//  Created by Vishva Patel on 3/16/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit

class Flights: NSObject {
    var id : Int?
    var name : String?
    
    func initWithData(theRow i : Int, theName n : String) {
        id = i
        name = n
    }
}
