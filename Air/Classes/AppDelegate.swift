//
//  AppDelegate.swift
//  Air
//
//  Created by Vishva Patel on 3/5/20.
//  Copyright © 2020 Vishva Patel. All rights reserved.
//

import UIKit
import GoogleSignIn
import SQLite3

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate{

    var window: UIWindow?
    var databaseName : String? = "MyFlights.db"
    var databasePath : String?
    
    var bookings : [Booking] = []
    var flights : [Flights] = []
    var newUsers : [NewUsers] = []
    var googleUser : [GUser] = []
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Initialize google sign-in
        GIDSignIn.sharedInstance().clientID = "83578550091-0rng8hpir3neqpb79c60vidm1qsv7l8p.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        let documentPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDir = documentPaths[0]
        databasePath = documentsDir.appending("/" + databaseName!)
        
        checkAndCreateDatabase()

        readDataFromDatabase()
        readDataFromDatabase1()
        readDataFromDatabase2()
               
        return true
    }
    
    func findUser(email: String, password: String) -> NewUsers {
        print("user is \(email)")
        var db : OpaquePointer?
         let user : NewUsers = NewUsers.init()
        
        if sqlite3_open(self.databasePath, &db) == SQLITE_OK{
            print("Successfully opened connection to \(String(describing: self.databasePath))")
                   
        var queryStatement : OpaquePointer?
        let queryStatementString = "select * from users where email='"+email + "' AND password='" + password + "';"
                  
            if sqlite3_prepare(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK{

                while sqlite3_step(queryStatement) == SQLITE_ROW{
                    let id : Int = Int(sqlite3_column_int(queryStatement, 0))
                    let cFirstName  = sqlite3_column_text(queryStatement, 1)
                    let cLastName  = sqlite3_column_text(queryStatement, 2)
                    let cEmail  = sqlite3_column_text(queryStatement, 3)
                    let cPassword  = sqlite3_column_text(queryStatement, 4)
                    let cRePassword  = sqlite3_column_text(queryStatement, 5)
                    let cContact  = sqlite3_column_text(queryStatement, 6)
                    
                    let firstname = String(cString: cFirstName!)
                    let lastname = String(cString: cLastName!)
                    let email = String(cString: cEmail!)
                    let password = String(cString: cPassword!)
                    let repassword = String(cString: cRePassword!)
                    let contact = String(cString: cContact!)
                    
                   
                    
                    user.initWithData(theRow: id, theFirstName: firstname, theLastName: lastname, theEmail: email, thePassword: password, theRePassword: repassword, theContact: contact)
                    newUsers.append(user)
                    print("Query Result from Users table")
                    print("\(id) | \(firstname) | \(lastname) | \(email) | \(contact) ")
                    
                    sqlite3_finalize(queryStatement)
                    return user
                }
                
            }else{
                let errorMessage = String(cString: sqlite3_errmsg(db)); print("\nQuery is not prepared! \(errorMessage)")
            }
            sqlite3_close(db)
        }else{
            print("Unbale to open database")
        }
        return user
    }
    
    //function to insert data into Users table every time a user register for our app.
    func insertIntoDatabase(data : NewUsers) -> Bool{
        
        var db : OpaquePointer? = nil
        let returnCode : Bool = true
        
        if sqlite3_open(self.databasePath, &db) == SQLITE_OK {
            
            var insertStatement : OpaquePointer? = nil
            let insertStatementString : String  = "Insert into users values (NULL, ?,?,?,?,?,?)"
            
            if sqlite3_prepare(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
                
                let firstnameStr = data.FirstName! as NSString
                let lastnameStr = data.LastName! as NSString
                let emailStr = data.Email! as NSString
                let passwordStr = data.Password! as NSString
                let repasswordStr  = data.RePassword! as NSString
                let contactStr = data.Contact! as NSString
                
                sqlite3_bind_text(insertStatement, 1, firstnameStr.utf8String, -1,nil)
                sqlite3_bind_text(insertStatement, 2, lastnameStr.utf8String, -1,nil)
                sqlite3_bind_text(insertStatement, 3, emailStr.utf8String, -1,nil)
                sqlite3_bind_text(insertStatement, 4, passwordStr.utf8String, -1,nil)
                sqlite3_bind_text(insertStatement, 5, repasswordStr.utf8String, -1,nil)
                sqlite3_bind_text(insertStatement, 6, contactStr.utf8String, -1,nil)
                
                if sqlite3_step(insertStatement) == SQLITE_DONE {
                    let rowID = sqlite3_last_insert_rowid(db)
                    print("Succeessfully Added \(rowID)")
                }
                else{
                    let errorMessage = String(cString: sqlite3_errmsg(db)); print("\nCouldnt Insert! \(errorMessage)")
                }
                sqlite3_finalize(insertStatement)
            }
            else{
             let errorMessage = String(cString: sqlite3_errmsg(db)); print("\nInsert statement could not be prepared! \(errorMessage)")
            }
            sqlite3_close(db)
        }
        else{
            let errorMessage = String(cString: sqlite3_errmsg(db)); print("\n  Unable to open database! \(errorMessage)")
        }
        return returnCode
    }
    
    //function to insert data into Bookings table
    func insertIntoDatabase2(bookings : Booking) -> Bool
    {
        var db: OpaquePointer? = nil
        var returnCode : Bool = true
        
        if sqlite3_open(self.databasePath, &db) == SQLITE_OK {
            print("Successfully opened connection to database at \(String(describing: self.databasePath))")
            
            var insertStatement: OpaquePointer? = nil
            let insertStatementString : String = "insert into bookings values(NULL, ?, ?, ?, ?, ?, ?)"
            
            if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
               
                let fromStr = bookings.countryfrom! as NSString
                let toStr = bookings.countryto! as NSString
                let departingStr = bookings.departingdate! as NSString
                let returningStr = bookings.returningdate! as NSString
                let classStr = bookings.flyclass! as NSString
                let travellersStr = bookings.travellers! as NSString
                
                sqlite3_bind_text(insertStatement, 1, fromStr.utf8String, -1, nil)
                sqlite3_bind_text(insertStatement, 2, toStr.utf8String, -1, nil)
                sqlite3_bind_text(insertStatement, 3, departingStr.utf8String, -1, nil)
                sqlite3_bind_text(insertStatement, 4, returningStr.utf8String, -1, nil)
                sqlite3_bind_text(insertStatement, 5, classStr.utf8String, -1, nil)
                sqlite3_bind_text(insertStatement, 6, travellersStr.utf8String, -1, nil)
                
                if sqlite3_step(insertStatement) == SQLITE_DONE {
                    let rowID = sqlite3_last_insert_rowid(db)
                    print("Successfully inserted row. \(rowID)")
                } else {
                    print("Could not insert row.")
                    returnCode = false
                }
                sqlite3_finalize(insertStatement)
            } else {
                print("INSERT statement could not be prepared.")
                returnCode = false
            }
            sqlite3_close(db);
        } else {
            print("Unable to open database.")
            returnCode = false
        }
        return returnCode
    }
    
    //function to read data from Flights table every time a user serach for flights
    func readDataFromDatabase(){
        flights.removeAll()
        
        var db : OpaquePointer?
        
        if sqlite3_open(self.databasePath, &db) == SQLITE_OK{
            print("Successfully opened connection to \(String(describing: self.databasePath))")
            
            var queryStatement : OpaquePointer?
            let queryStatementString : String? = "Select * from flights";
           
            if sqlite3_prepare(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK{

                while sqlite3_step(queryStatement) == SQLITE_ROW{
                    let id : Int = Int(sqlite3_column_int(queryStatement, 0))
                    let cname  = sqlite3_column_text(queryStatement, 1)

                    let name = String(cString: cname!)
                    let flight : Flights = Flights.init()
                    flight.initWithData(theRow: id, theName: name)
                    flights.append(flight)

                    print("Query Result from Flights Table")
                    print("\(id) | \(name)")

                }
                sqlite3_finalize(queryStatement)
            }else{
                print("Select statement couldnot be prepared")
            }
            sqlite3_close(db)
        }else{
            print("Unbale to open database")
        }
    }
    
    //function to read data from users table.
    func readDataFromDatabase1(){
        newUsers.removeAll()
        
        var db : OpaquePointer?
        
        if sqlite3_open(self.databasePath, &db) == SQLITE_OK{
            print("Successfully opened connection to \(String(describing: self.databasePath))")
                   
        var queryStatement : OpaquePointer?
                   
        let queryStatementString : String? = "select * from users";
                  
            if sqlite3_prepare(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK{

                while sqlite3_step(queryStatement) == SQLITE_ROW{
                    let id : Int = Int(sqlite3_column_int(queryStatement, 0))
                    let cFirstName  = sqlite3_column_text(queryStatement, 1)
                    let cLastName  = sqlite3_column_text(queryStatement, 2)
                    let cEmail  = sqlite3_column_text(queryStatement, 3)
                    let cPassword  = sqlite3_column_text(queryStatement, 4)
                    let cRePassword  = sqlite3_column_text(queryStatement, 5)
                    let cContact  = sqlite3_column_text(queryStatement, 6)
                    
                    let firstname = String(cString: cFirstName!)
                    let lastname = String(cString: cLastName!)
                    let email = String(cString: cEmail!)
                    let password = String(cString: cPassword!)
                    let repassword = String(cString: cRePassword!)
                    let contact = String(cString: cContact!)
                    
                    let user : NewUsers = NewUsers.init()
                    
                    user.initWithData(theRow: id, theFirstName: firstname, theLastName: lastname, theEmail: email, thePassword: password, theRePassword: repassword, theContact: contact)
                    newUsers.append(user)
                    print("Query Result from Users table")
                    print("\(id) | \(firstname) | \(lastname) | \(email) | \(password) | \(contact)")

                }
                sqlite3_finalize(queryStatement)
            }else{
                let errorMessage = String(cString: sqlite3_errmsg(db)); print("\nQuery is not prepared! \(errorMessage)")
            }
            sqlite3_close(db)
        }else{
            print("Unbale to open database")
        }
    }
    
    //function for reading data from Bookings table to retrive bookings done by user.
    func readDataFromDatabase2(){
        bookings.removeAll()
        
        var db : OpaquePointer?
        
        if sqlite3_open(self.databasePath, &db) == SQLITE_OK{
            print("Successfully opened connection to \(String(describing: self.databasePath))")
            
            var queryStatement : OpaquePointer?
            
            let queryStatementString : String? = "Select * from bookings";
           
            if sqlite3_prepare(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK{

                while sqlite3_step(queryStatement) == SQLITE_ROW{
                    let id: Int = Int(sqlite3_column_int(queryStatement, 0))
                    let cfrom = sqlite3_column_text(queryStatement, 1)
                    let cto = sqlite3_column_text(queryStatement, 2)
                    let cdeparting = sqlite3_column_text(queryStatement, 3)
                    let creturning = sqlite3_column_text(queryStatement, 4)
                    let cflyclass = sqlite3_column_text(queryStatement, 5)
                    let ctravellers = sqlite3_column_text(queryStatement, 6)

                    let from = String(cString: cfrom!)
                    let to = String(cString: cto!)
                    let departing = String(cString: cdeparting!)
                    let returning = String(cString: creturning!)
                    let flyclass = String(cString: cflyclass!)
                    let travellers = String(cString: ctravellers!)
                    
                    let booking : Booking = Booking.init()
                    booking.initWithData(theRow: id, thecountryfrom: from, thecountryto: to, thedepartingdate: departing, thereturningdate: returning, theflyclass: flyclass, thetravellers: travellers)
                    bookings.append(booking)

                    print("Query Result from Bookings Table")
                    print("\(id) | \(from) | \(to) | \(departing) | \(returning) | \(flyclass) | \(travellers)")

                }
                sqlite3_finalize(queryStatement)
            }else{
                print("Select statement couldnot be prepared")
            }
            sqlite3_close(db)
        }else{
            print("Unbale to open database")
        }
    }
    
    func checkAndCreateDatabase(){
        var success = false
        let fileManager  = FileManager.default
        success = fileManager.fileExists(atPath: databasePath!)
        if success{
            return
        }
        let databasePathFromApp = Bundle.main.resourcePath?.appending("/" + databaseName!)
        try? fileManager.copyItem(atPath: databasePathFromApp!, toPath: databasePath!)
         
        return
    }
    
    //implementing Google SignIn into our app
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        login()
        // ...
    }
    
    func login () {
           
           let board = UIStoryboard(name: "Main", bundle: nil)

           let Page = board.instantiateViewController(withIdentifier: "MainPage")

    window?.rootViewController = Page
       }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


